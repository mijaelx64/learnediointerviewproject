import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Home from "../pages/home";
import UpdatePost, { UpdatePostParams } from "../pages/updatePost";
import CreatePost from "../pages/createPost";

export type RouteParamList = {
  Home: undefined;
  UpdatePost: UpdatePostParams;
  CreatePost: undefined;
};

const Stack = createNativeStackNavigator<RouteParamList>();
const RootNavigation = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="Home"
      component={Home}
      options={{
        title: "POST PROJECT",
      }}
    />
    <Stack.Screen
      name="UpdatePost"
      component={UpdatePost}
      options={{
        title: "Update Post",
      }}
    />
    <Stack.Screen
      name="CreatePost"
      component={CreatePost}
      options={{
        title: "Create New Post",
      }}
    />
  </Stack.Navigator>
);

export default RootNavigation;
