import React, { FC, useEffect } from "react";
import { UpdateForm } from "../../components/templates";
import Post from "../../model/typicodeApi/Post";
import { RouteProp } from "@react-navigation/native";
import { RouteParamList } from "../../navigation/rootNavigation";
import { StackNavigationProp } from "@react-navigation/stack";
import { useUpdatePostMutation } from "../../redux/services/typicode/typicodeSlice";
import { ActivityIndicator, View } from "react-native";

// Interfaces and types
export type UpdatePostProps = {
  route: RouteProp<RouteParamList, "UpdatePost">;
  navigation: StackNavigationProp<RouteParamList, "UpdatePost">;
};
export interface UpdatePostParams {
  data: Post;
}

// Component
const UpdatePost: FC<UpdatePostProps> = ({ route, navigation }) => {
  const { data } = route.params;

  const [updatePost, { isLoading, isSuccess }] = useUpdatePostMutation();

  useEffect(() => {
    if (isSuccess) {
      navigation.goBack();
    }
  }, [isSuccess]);

  return (
    <View>
      <ActivityIndicator size="large" animating={isLoading} />
      <UpdateForm
        postId={data.id}
        currentTitle={data.title}
        currentBody={data.body}
        onUpdate={(item) => {
          updatePost(item);
        }}
        onCancel={() => {
          navigation.goBack();
        }}
      ></UpdateForm>
    </View>
  );
};

export default UpdatePost;
