import React, { FC, useEffect, useState } from "react";
import styled from "styled-components/native";
import {
  useDeletePostMutation,
  useGetPostsQuery,
} from "../../redux/services/typicode/typicodeSlice";
import SearchBar from "../../components/atoms/searchBar";
import Post from "../../model/typicodeApi/Post";
import { PostItem } from "../../components/molecules";
import {
  ActivityIndicator,
  Button,
  FlatList,
  ScrollView,
  View,
} from "react-native";
import { RouteProp } from "@react-navigation/native";
import { RouteParamList } from "../../navigation/rootNavigation";
import { StackNavigationProp } from "@react-navigation/stack";

export type HomeProps = {
  route: RouteProp<RouteParamList, "Home">;
  navigation: StackNavigationProp<RouteParamList, "Home">;
};

const Container = styled.View`
  flex: 1;
  background-color: #fff;
`;

const CreatePostButton = styled.Button`
  background-color: #00008b;
  width: 50px;
  height: 50px;
  position: absolute;
  bottom: 16px;
  right: 16px;
  align-content: center;
`;

const Home: FC<HomeProps> = ({ navigation }) => {
  const [searchText, setSearchText] = useState("");

  const { data } = useGetPostsQuery(searchText);

  const [posts, setPosts] = useState<Post[]>([]);

  const [deletePost, { isLoading: isLoadingDeletePost }] =
    useDeletePostMutation();
  const onSearch = (searchText: string) => {
    setSearchText(searchText);
  };

  const onUpdate = (post: Post) => {
    navigation.navigate("UpdatePost", { data: post });
  };

  const onDelete = (id: number) => {
    deletePost(id);
  };

  useEffect(() => {
    if (data && searchText === "") {
      setPosts(data);
    }
    if (data && searchText !== "") {
      const filteredData = data.filter((item) =>
        item.title.toLowerCase().includes(searchText.toLowerCase()),
      );
      setPosts(filteredData);
    }
  }, [data, searchText]);

  return (
    <Container>
      <SearchBar onSearch={onSearch} initValue={""} />
      {isLoadingDeletePost && (
        <ActivityIndicator size={"large"} animating={isLoadingDeletePost} />
      )}

      <CreatePostButton
        title="+ Create New Post"
        onPress={() => navigation.navigate("CreatePost")}
      />

      <Container>
        {posts && (
          <FlatList
            data={posts}
            renderItem={({ item, index }) => (
              <PostItem
                key={`key-${index}`}
                post={item}
                onDelete={onDelete}
                onUpdate={onUpdate}
              />
            )}
            keyExtractor={(item) => item.id.toString()}
          />
        )}
      </Container>
    </Container>
  );
};

export default Home;
