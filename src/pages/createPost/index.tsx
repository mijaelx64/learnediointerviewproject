import React, { FC, useEffect } from "react";
import { UpdateForm } from "../../components/templates";
import Post from "../../model/typicodeApi/Post";
import { RouteProp } from "@react-navigation/native";
import { RouteParamList } from "../../navigation/rootNavigation";
import { StackNavigationProp } from "@react-navigation/stack";
import {
  useCreatePostMutation,
  useUpdatePostMutation,
} from "../../redux/services/typicode/typicodeSlice";
import { ActivityIndicator, View } from "react-native";

// Interfaces and types
export type CreatePostProps = {
  route: RouteProp<RouteParamList, "CreatePost">;
  navigation: StackNavigationProp<RouteParamList, "CreatePost">;
};

// Component
const CreatePost: FC<CreatePostProps> = ({ route, navigation }) => {
  const [createPost, { isLoading, isSuccess }] = useCreatePostMutation();

  useEffect(() => {
    if (isSuccess) {
      navigation.goBack();
    }
  }, [isSuccess]);

  return (
    <View>
      <ActivityIndicator size="large" animating={isLoading} />
      <UpdateForm
        postId={10000}
        currentTitle={""}
        currentBody={""}
        onUpdate={(item) => {
          createPost(item);
        }}
        onCancel={() => {
          navigation.goBack();
        }}
        submitTitle={"Create New Post"}
      ></UpdateForm>
    </View>
  );
};

export default CreatePost;
