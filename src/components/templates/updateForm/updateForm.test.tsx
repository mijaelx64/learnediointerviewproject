import React from "react";
import { render, fireEvent, waitFor } from "@testing-library/react-native";
import renderer from "react-test-renderer";
import { act } from "react-test-renderer";
import UpdateForm from "./index";

describe("UpdateForm component", () => {
  const postId = 1;
  const currentTitle = "Current Title";
  const currentBody = "Current Body";
  const onUpdateMock = jest.fn();
  const onCancelMock = jest.fn();

  it("renders correctly", () => {
    const tree = renderer
      .create(
        <UpdateForm
          postId={postId}
          currentTitle={currentTitle}
          currentBody={currentBody}
          onUpdate={onUpdateMock}
          onCancel={onCancelMock}
        />,
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("calls onUpdate with the correct values when Update button is pressed", async () => {
    const { getByPlaceholderText, getByText } = render(
      <UpdateForm
        postId={postId}
        currentTitle={currentTitle}
        currentBody={currentBody}
        onUpdate={onUpdateMock}
        onCancel={onCancelMock}
      />,
    );

    const newTitleInput = getByPlaceholderText("New Title");
    const newBodyInput = getByPlaceholderText("New Body");
    const updateButton = getByText("Update");

    fireEvent.changeText(newTitleInput, "New Test Title");
    fireEvent.changeText(newBodyInput, "New Test Body");

    act(() => {
      fireEvent.press(updateButton);
    });

    await waitFor(() => {
      expect(onUpdateMock).toHaveBeenCalledWith({
        title: "New Test Title",
        body: "New Test Body",
        id: postId,
        userId: 1,
      });
    });
  });

  it("calls onCancel when Cancel button is pressed", () => {
    const { getByText } = render(
      <UpdateForm
        postId={postId}
        currentTitle={currentTitle}
        currentBody={currentBody}
        onUpdate={onUpdateMock}
        onCancel={onCancelMock}
      />,
    );

    const cancelButton = getByText("Cancel");
    fireEvent.press(cancelButton);

    expect(onCancelMock).toHaveBeenCalled();
  });
});
