import React from "react";
import { Button } from "react-native";
import styled from "styled-components/native";
import { Formik, Field } from "formik";
import * as Yup from "yup";
import Post from "../../../model/typicodeApi/Post";

interface UpdateFormProps {
  postId: number;
  currentTitle: string;
  currentBody: string;
  onUpdate: (item: Post) => void;
  onCancel: () => void;
  submitTitle?: string;
}

const Container = styled.View`
  padding: 16px;
`;

const Input = styled.TextInput`
  min-height: 40px;
  border-color: gray;
  border-width: 1px;
  border-radius: 8px;
  margin-bottom: 12px;
  padding: 8px;
`;

const ValidationText = styled.Text`
  color: red;
`;

const ButtonContainer = styled.View`
  flex-direction: row;
  justify-content: space-around;
`;

const validationSchema = Yup.object().shape({
  newTitle: Yup.string().required("Title is required"),
  newBody: Yup.string().required("Body is required"),
});

const UpdateForm: React.FC<UpdateFormProps> = ({
  postId,
  currentTitle,
  currentBody,
  onUpdate,
  onCancel,
  submitTitle = "Update",
}) => {
  const initialValues = {
    newTitle: currentTitle,
    newBody: currentBody,
  };

  const handleUpdate = (values: { newTitle: string; newBody: string }) => {
    onUpdate({
      title: values.newTitle,
      body: values.newBody,
      id: postId,
      userId: 1,
    });
  };

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={handleUpdate}
    >
      {({
        handleChange,
        handleBlur,
        handleSubmit,
        values,
        errors,
        touched,
      }) => (
        <Container>
          <Field
            component={Input}
            name="newTitle"
            placeholder="New Title"
            onChangeText={handleChange("newTitle")}
            onBlur={handleBlur("newTitle")}
            value={values.newTitle}
          />
          {touched.newTitle && errors.newTitle && (
            <ValidationText>{errors.newTitle}</ValidationText>
          )}
          <Field
            component={Input}
            name="newBody"
            placeholder="New Body"
            multiline
            onChangeText={handleChange("newBody")}
            onBlur={handleBlur("newBody")}
            value={values.newBody}
          />
          {touched.newBody && errors.newBody && (
            <ValidationText>{errors.newBody}</ValidationText>
          )}

          <ButtonContainer>
            <Button title={`${submitTitle}`} onPress={() => handleSubmit()} />
            <Button title="Cancel" onPress={onCancel} color="red" />
          </ButtonContainer>
        </Container>
      )}
    </Formik>
  );
};

export default UpdateForm;
