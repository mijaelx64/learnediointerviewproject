import React from "react";
import {
  Card,
  Title as PaperTitle,
  Paragraph,
  Button,
} from "react-native-paper";
import styled from "styled-components/native";
import { MaterialIcons } from "@expo/vector-icons";
import Post from "../../../model/typicodeApi/Post";
import { View } from "react-native";

interface PostItemProps {
  post: {
    userId: number;
    id: number;
    title: string;
    body: string;
  };
  onUpdate: (post: Post) => void;
  onDelete: (id: number) => void;
}

const StyledCard = styled(Card)`
  margin: 16px;
`;

const Id = styled.Text`
  font-size: 14px;
  color: #3d5afe;
  margin-bottom: 4px;
`;

const Title = styled(PaperTitle)`
  font-size: 18px;
  font-weight: bold;
  color: #3d5afe;
`;

const Body = styled(Paragraph)`
  font-size: 16px;
  color: #333;
`;

const ActionButtonsContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-top: 8px;
`;

const ActionButton = styled(Button)`
  margin: 4px;
`;

const PostItem: React.FC<PostItemProps> = ({ post, onUpdate, onDelete }) => {
  return (
    <View key={post.id}>
      <StyledCard>
        <Card.Content>
          <Id>ID: {post.id}</Id>
          <Title>{post.title}</Title>
          <Body>{post.body}</Body>
          <ActionButtonsContainer>
            <ActionButton
              mode="contained"
              icon={() => <MaterialIcons name="edit" size={20} color="white" />}
              onPress={() => onUpdate(post)}
            >
              Update
            </ActionButton>
            <ActionButton
              mode="contained"
              icon={() => (
                <MaterialIcons name="delete" size={20} color="white" />
              )}
              onPress={() => onDelete(post.id)}
            >
              Delete
            </ActionButton>
          </ActionButtonsContainer>
        </Card.Content>
      </StyledCard>
    </View>
  );
};

export default PostItem;
