import React from "react";
import { render, fireEvent, RenderAPI } from "@testing-library/react-native";
import renderer from "react-test-renderer";
import PostItem from "./index";

describe("PostItem component", () => {
  const post = {
    userId: 1,
    id: 101,
    title: "Test Title",
    body: "Test Body",
  };

  const onUpdateMock = jest.fn();
  const onDeleteMock = jest.fn();

  it("renders correctly", () => {
    const tree = renderer
      .create(
        <PostItem
          post={post}
          onUpdate={onUpdateMock}
          onDelete={onDeleteMock}
        />,
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("calls onUpdate when update button is pressed", () => {
    const { getByText } = render(
      <PostItem post={post} onUpdate={onUpdateMock} onDelete={onDeleteMock} />,
    );
    const updateButton = getByText("Update");
    fireEvent.press(updateButton);

    expect(onUpdateMock).toHaveBeenCalledWith(post);
  });

  it("calls onDelete when delete button is pressed", () => {
    const { getByText } = render(
      <PostItem post={post} onUpdate={onUpdateMock} onDelete={onDeleteMock} />,
    );
    const deleteButton = getByText("Delete");
    fireEvent.press(deleteButton);

    expect(onDeleteMock).toHaveBeenCalledWith(post.id);
  });
});
