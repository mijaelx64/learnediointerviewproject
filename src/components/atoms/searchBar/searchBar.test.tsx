import React from 'react';
import { render, fireEvent, RenderAPI } from '@testing-library/react-native';
import SearchBar from './index';
import renderer from 'react-test-renderer';

describe('SearchBar component', () => {
    it('renders correctly', () => {
        const tree = renderer.create(<SearchBar onSearch={() => {}} />).toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('calls onSearch with the correct search text when submitted', () => {
        const onSearchMock = jest.fn();
        const { getByPlaceholderText, getByTestId } = render(
            <SearchBar onSearch={onSearchMock} />
    );

        const searchText = 'test search';
        const searchInput = getByPlaceholderText('Search...') as RenderAPI['getByPlaceholderText'];
        fireEvent.changeText(searchInput, searchText);

        const searchButton = getByTestId('search-button') as RenderAPI['getByTestId'];
        fireEvent.press(searchButton);

        expect(onSearchMock).toHaveBeenCalledWith(searchText);
    });
});
