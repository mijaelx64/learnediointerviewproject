import React, { useState } from "react";
import styled from "styled-components/native";
import { Ionicons } from "@expo/vector-icons";

interface SearchBarProps {
  onSearch: (searchText: string) => void;
  initValue?: string;
}

const SearchBarContainer = styled.View`
  flex-direction: row;
  align-items: center;
  background-color: #e0e0e0;
  padding: 10px;
  border-radius: 20px;
  margin: 10px;
`;

const SearchInput = styled.TextInput`
  flex: 1;
  margin-right: 8px;
  margin-left: 8px;
`;

const SearchBar: React.FC<SearchBarProps> = ({ onSearch, initValue = "" }) => {
  const [searchText, setSearchText] = useState(initValue);

  const handleSearch = () => {
    onSearch(searchText);
  };

  return (
    <SearchBarContainer>
      <SearchInput
        placeholder="Search..."
        value={searchText}
        onChangeText={(text) => {
          setSearchText(text);
          onSearch(text);
        }}
        onSubmitEditing={handleSearch}
      />
      <Ionicons
        testID="search-button"
        name="search"
        size={24}
        color="black"
        onPress={handleSearch}
      />
    </SearchBarContainer>
  );
};

export default SearchBar;
