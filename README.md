# Project Name

Brief and concise description of the project.

## Prerequisites

Before you begin, make sure you clone your project and install all the dependencies.

```bash
npm install | yarn install
```

## Run project ==>  IOS

For running the project in development mode, you can use the following commands for IOS:

```bash
npm run ios | yarn ios
```

## Run project ==> ANDROID

For running the project in development mode, you can use the following commands for Android:

```bash
npm run android | yarn android
```

## Run Tests with jest

For running the project in development mode, you can use the following commands for Android:

```bash
npm run test | yarn test
```


be sure to have an emulator running or a device connected.
