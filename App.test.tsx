import "react-native";
import React from "react";
import renderer from "react-test-renderer";
import App from "./App";

jest.mock("@react-navigation/native", () => ({
  NavigationContainer: ({ children }) => children,
}));

jest.mock("@react-navigation/native-stack", () => ({
  createNativeStackNavigator: jest.fn(),
}));

jest.mock("@react-native-community/async-storage", () => ({
  getItem: jest.fn(),
  setItem: jest.fn(),
  removeItem: jest.fn(),
}));

describe("App component", () => {
  it("renders correctly", () => {
    const tree = renderer.create("<App />").toJSON();
    expect(tree).toMatchSnapshot();
  });
});
