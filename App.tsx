import { StatusBar } from "expo-status-bar";
import styled from "styled-components/native";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

// Redux imports
import { store } from "./src/redux/store";
import { Provider } from "react-redux";
import Navigation from "./src/navigation";

const Container = styled.View`
  flex: 1;
`;

const App = () => {
  return (
    <Provider store={store}>
      <Container>
        <Navigation />
        <StatusBar style="auto" />
      </Container>
    </Provider>
  );
};
export default App;
